//
//  ViewController.swift
//  ParticleIOSStarter
//
//  Created by Parrot on 2019-06-29.
//  Copyright © 2019 Parrot. All rights reserved.

import UIKit
import Particle_SDK
import Speech

class ViewController: UIViewController {

    var gameScore = 0
    // MARK: User variables
    let USERNAME = ProcessInfo.processInfo.environment["USERNAME"]
    let PASSWORD = ProcessInfo.processInfo.environment["PASSWORD"]
    
    // MARK: Device
    let DEVICE_ID = ProcessInfo.processInfo.environment["DEVICE_ID"]
    var myPhoton : ParticleDevice?
    //MARK: IBOutlets
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var commandLbl: UILabel!
    @IBOutlet weak var stopBtn: UIButton!
    
    //Mark: Speech Recognition:
    let audioEngine = AVAudioEngine()
    let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-CA"))
    var recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask : SFSpeechRecognitionTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME!, password: self.PASSWORD!) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")

                // try to get the device
                self.getDeviceFromCloud()

            }
        } // end login
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Get Device from Cloud
    // Gets the device from the Particle Cloud
    // and sets the global device variable
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID!) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device!.id)")
                self.myPhoton = device
                
                // subscribe to events
                self.subscribeToParticleEvents()
            }
            
        } // end getDevice()
    }
    
    
    //MARK: Subscribe to "playerChoice" events on Particle
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "playerChoice",
            deviceID:self.DEVICE_ID!,
            handler: {
                (event :ParticleEvent?, error : Error?) in

            if let _ = error {
                print("could not subscribe to events")
            } else {
                print("got event with data \(event?.data)")
            }
        })
    }
    
    
    //MARK: IBActions
    @IBAction func startBtnPressed(_ sender: UIButton) {
        startBtn.isEnabled = false
        stopBtn.isEnabled = true
        recognizeSpeech()
    }
    
    @IBAction func stopBtnPressed(_ sender: UIButton) {
        startBtn.isEnabled = true
        stopBtn.isEnabled = false
        stopRecognizing()
    }
    
    func stopRecognizing() {
        self.commandLbl.text = ""
        self.commandLbl.backgroundColor = UIColor.white
        audioEngine.stop()
        recognitionRequest.endAudio()
        recognitionTask?.cancel()
        audioEngine.inputNode.removeTap(onBus: 0)
    }
    
    func recognizeSpeech() {
        let node = audioEngine.inputNode
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        recognitionRequest.shouldReportPartialResults = true
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, _) in
            self.recognitionRequest.append(buffer)
        }
        
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            return print(error)
        }
        
        guard let recognizeMe = SFSpeechRecognizer() else {
            return
        }
        
        if !recognizeMe.isAvailable {
            return
        }
        
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            if let result = result {
                let transcribedString = result.bestTranscription.formattedString
                self.commandLbl.text = transcribedString
                
                self.voiceCommand(command: transcribedString)
            } else if let error = error {
                print(error)
            }
        })
    }
    
    func voiceCommand (command: String) {
        switch command {
        case "Read", "Red":
            self.commandLbl.backgroundColor = UIColor.red
            self.callParticleFunc(functionName: "lightsColour", arg: ["red"])
            self.commandLbl.text = "Red"
        case "Green":
            self.commandLbl.backgroundColor = UIColor.green
            self.callParticleFunc(functionName: "lightsColour", arg: ["green"])
        case "Blue":
            self.commandLbl.backgroundColor = UIColor.blue
            self.callParticleFunc(functionName: "lightsColour", arg: ["blue"])
        case "Purple":
            self.commandLbl.backgroundColor = UIColor.purple
            self.callParticleFunc(functionName: "lightsColour", arg: ["purple"])
        case "Pink":
            self.commandLbl.backgroundColor = UIColor.magenta
            self.callParticleFunc(functionName: "lightsColour", arg: ["pink"])
        case "Yellow":
            self.commandLbl.backgroundColor = UIColor.yellow
            self.callParticleFunc(functionName: "lightsColour", arg: ["yellow"])
        case "Increase seconds", "Plus", "More":
            self.commandLbl.backgroundColor = UIColor.gray
            self.callParticleFunc(functionName: "lightsSeconds", arg: ["inc"])
        case "Decrease seconds", "Degree seconds", "Minus", "Less":
            self.commandLbl.backgroundColor = UIColor.gray
            self.callParticleFunc(functionName: "lightsSeconds", arg: ["dec"])
        case "Rainbow":
            self.commandLbl.backgroundColor = UIColor.cyan
            self.callParticleFunc(functionName: "lightsColour", arg: ["rainbow"])
        default:
//            print("DEFAULTTTTT")
//            self.commandLbl.textColor = UIColor.white
//            self.commandLbl.backgroundColor = UIColor.black
//            self.commandLbl.text = "Say it again!"
            self.stopRecognizing()
            self.recognizeSpeech()
        }
    }
    
    func callParticleFunc(functionName: String, arg: [String])
    {
        myPhoton!.callFunction(functionName, withArguments: arg) { (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                self.stopRecognizing()
                self.recognizeSpeech()
            }
        }
    }
}

